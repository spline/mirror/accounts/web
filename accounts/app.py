from flask import Flask, current_app
from flask_login import LoginManager
from typing import TYPE_CHECKING, cast

if TYPE_CHECKING:
    from .backend import user, mail


class AccountsFlask(Flask):
    username_blacklist: list[str]
    user_backend: "user.Backend"
    mail_backend: "mail.Backend"
    login_manager: LoginManager


accounts_app: AccountsFlask = cast(AccountsFlask, current_app)
