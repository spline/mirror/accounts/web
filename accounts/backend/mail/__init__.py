# -*- coding: utf-8 -*-

from flask.app import Flask
from jinja2 import Template
from jinja2.environment import TemplateModule


class Backend:
    app: Flask

    def __init__(self, app: Flask) -> None:
        self.app = app

    def _send(self, recipient: str, content: TemplateModule):
        raise NotImplementedError()

    def send(self, recipient: str, template, **kwargs):
        if recipient is None:
            return

        tmpl: Template = self.app.jinja_env.get_or_select_template(template)

        kwargs["recipient"] = recipient
        module = tmpl.make_module(kwargs)

        self._send(recipient, module)
