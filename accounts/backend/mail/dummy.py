# -*- coding: utf-8 -*-


from . import Backend
from accounts import AccountsFlask
from jinja2.environment import TemplateModule


FANCY_FORMAT = """\
,---------------------------------------------------------------------------
| Subject: {subject}
| To: {to}
| From: {sender}
|---------------------------------------------------------------------------
| {body}
`---------------------------------------------------------------------------"""


PLAIN_FORMAT = """Subject: {subject}
To: {to}
From: {sender}

{body}"""


class DummyBackend(Backend):
    format: str

    def __init__(
        self, app: AccountsFlask, plain: bool = False, format: None = None
    ) -> None:
        super(DummyBackend, self).__init__(app)
        self.plain = plain

        if format is None:
            if self.plain:
                self.format = PLAIN_FORMAT
            else:
                self.format = FANCY_FORMAT
        else:
            self.format = format

    def _send(self, recipient: str, content: TemplateModule):
        # we can't typecheck things defined in templates
        body = content.body()  # type: ignore
        if not self.plain:
            body = "\n| ".join(body.split("\n"))

        print(
            self.format.format(
                subject=content.subject(),  # type: ignore
                sender=content.sender(),  # type: ignore
                to=recipient,
                body=body,
            )
        )
