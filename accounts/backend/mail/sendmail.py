# -*- coding: utf-8 -*-


import subprocess
from email.mime.text import MIMEText
from email.utils import parseaddr
from jinja2.environment import TemplateModule

from . import Backend


def safe(s: str):
    return s.split("\n", 1)[0]


class SendmailBackend(Backend):
    def _send(self, recipient: str, content: TemplateModule):
        msg = MIMEText(content.body(), _charset="utf-8")  # type: ignore
        msg["Subject"] = safe(content.subject())  # type: ignore
        msg["To"] = safe(recipient)
        msg["From"] = safe(content.sender())  # type: ignore

        envelope = []
        _, address = parseaddr(safe(content.sender()))  # type: ignore
        if address != "":
            envelope = ["-f", address]

        p = subprocess.Popen(
            [self.app.config["SENDMAIL_COMMAND"]] + envelope + ["-t"],
            stdin=subprocess.PIPE,
        )
        assert p.stdin
        p.stdin.write(msg.as_string().encode("utf-8"))
        p.stdin.close()

        if p.wait() != 0:
            raise RuntimeError("sendmail terminated with %d" % p.returncode)
