from datetime import timedelta

DEBUG = False

SECRET_KEY = "remember to change this to something more random and secret"

# CHANGE THIS! (e.g. os.urandom(32) )
SESSION_ENCRYPTION_KEY = b".\x14\xa7\x1b\xa2:\x1b\xb7\xbck\x1bD w\xab\x87a\xb4\xb7\xca\xf1\x06\xb0\x9f?q\x13\x05\x8dY\xe5<"

MAIL_DEFAULT_SENDER = "spline accounts <noreply@accounts.spline.de>"
MAIL_REGISTER_NOTIFY = None
DISABLED_ACCOUNT_MAILADDRESS_TEMPLATE = (
    "noreply-disabledaccount-%s@accounts.spline.de"
)

SENDMAIL_COMMAND = "/usr/sbin/sendmail"

# to make the cookie a session cookie, set this to None
PERMANENT_SESSION_LIFETIME = timedelta(seconds=600)  # 10 minutes

LDAP_HOST = "ldap://localhost:5678"
LDAP_BASE_DN = [
    ("dc", "accounts"),
    ("dc", "spline"),
    ("dc", "inf"),
    ("dc", "fu-berlin"),
    ("dc", "de"),
]
LDAP_ADMIN_USER = "admin"
LDAP_ADMIN_PASS = "admin"

PREFERRED_URL_SCHEME = "https"

USER_BACKEND = "accounts.backend.user.dummy"
MAIL_BACKEND = "accounts.backend.mail.dummy"

ADMIN_USERS = ["admin"]

SERVICES = [
    ("Service1", "https://service1.spline.de"),
    ("Service2", "https://service2.spline.de"),
]
