from flask_login import UserMixin

from typing import Any, Optional

from accounts.utils.login import create_userid


class Account(UserMixin):
    """
    An Account represents a complex ldap tree entry for spline users.
    For each service a spline user can have a different password.
    """

    _ready = False

    uid: str
    password: Optional[str]
    attributes: dict[Any, Any]
    new_password_root: Optional[tuple[Optional[str], Optional[str]]]

    def __init__(
        self,
        uid: str,
        mail: str,
        password: Optional[str] = None,
        uidNumber=None,
    ):
        self.uid = uid
        self.password = password
        self.new_password_root = None
        self.attributes = {}
        self.uidNumber = uidNumber

        self._set_attribute("mail", mail)
        self._ready = True

    def __repr__(self):
        return "<Account uid=%s>" % self.uid

    def change_password(
        self, new_password: str, old_password=""
    ):
        """
        Changes the password of the account
        """

        self.new_password_root = (old_password, new_password)

    def _set_attribute(self, key: str, value: Any) -> None:
        self.attributes[key] = value

    def change_email(self, new_mail: str) -> None:
        """
        Changes the mail address of an account. You have to use the
        AccountService class to make changes permanent.
        """
        self._set_attribute("mail", new_mail)

    def __getattr__(self, name):
        if name in self.attributes:
            return self.attributes[name]

        raise AttributeError(
            "'%s' object has no attribute '%s'"
            % (self.__class__.__name__, name)
        )

    def __setattr__(self, name: str, value: Any):
        if self._ready and name not in self.__dict__:
            self._set_attribute(name, value)
        else:
            super(Account, self).__setattr__(name, value)

    def get_id(self):
        """
        This is for flask-login. The returned string is saved inside
        the cookie and used to identify the user.
        """
        return create_userid(self.uid, self.password)
