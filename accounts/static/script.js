function settings_service_toggle() {

    $('.service').each(function() {
      var header = $(this).find('h3');
      var list = $(this).find('ul');
      var content = $(this).find('.form-service');
      var elem = $('<a class="service-edit" href="">bearbeiten</a>')

      elem.click(function() {
        content.toggle();

        return false;
      });

      if(list.find('a').length > 0) {
        list.find('a').parent().prepend(elem);
      } else {
        var li = $('<li></li>');
        li.append(elem);
        list.append(li);
      }
      
      content.hide();
    });

  }
