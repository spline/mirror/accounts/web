{% extends 'mail/_base.txt' %}

{% block subject %}E-Mail-Adresse bestätigen{% endblock %}
{% block sender %}{{ config['MAIL_CONFIRM_SENDER'] }}{% endblock %}

{% block body -%}
Hallo,

Jemand, vermutlich du, möchte auf spline accounts [1] die
E-Mail-Adresse des Accounts {{ username }} auf diese Adresse
    {{ recipient }}
ändern.

Um diese Änderung zu bestätigen, benutze bitte folgenden Link:
    <{{ url_for('default.change_mail', _external=True,
                token=confirm('change_mail', username, recipient)) }}>


Wenn du dies nicht möchtest, brauchst du nichts weiter zu tun.
Ohne deine Bestätigung wird die Adresse nicht geändert.


[1] {{ url_for('default.index', _external=True) }}
{%- endblock %}
