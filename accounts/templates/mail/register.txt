{% extends 'mail/_base.txt' %}

{% block subject %}E-Mail-Adresse bestätigen{% endblock %}
{% block sender %}{{ config['MAIL_CONFIRM_SENDER'] }}{% endblock %}

{% block body -%}
Hallo,

Jemand, vermutlich du, möchte auf spline accounts [1] einen Account
mit folgenden Daten anlegen:

    Benutzername:   {{ username }}
    E-Mail-Adresse: {{ recipient }}


Wenn du diesen Account anlegen möchtest, bestätige mit folgendem Link
deine E-Mail-Adresse:
    <{{ url_for('default.register_complete', _external=True,
                token=confirm('register', username, recipient)) }}>


Wenn du diesen Account nicht anlegen möchtest, brauchst du nichts
weiter zu tun. Ohne deine Bestätigung wird der Account nicht erstellt.


[1] {{ url_for('default.index', _external=True) }}
{%- endblock %}
