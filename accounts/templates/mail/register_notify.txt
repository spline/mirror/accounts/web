{% extends 'mail/_base.txt' %}

{% block subject -%}
  [accounts] Neuer Benutzer {{ username }} erstellt
{%- endblock %}

{% block body -%}
Benutzername: {{ username }}
E-Mail:       {{ mail }}

Spammer? Deaktivieren: {{ url_for('admin.disable_account', uid=username,
                                  _external=True) }}
{%- endblock %}
