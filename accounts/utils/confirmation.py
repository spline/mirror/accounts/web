# -*- coding: utf-8 -*-
from itsdangerous import BadSignature, SignatureExpired, URLSafeTimedSerializer
from werkzeug.exceptions import Forbidden
from accounts.app import accounts_app

from typing import Union, Optional, Any


class Confirmation(URLSafeTimedSerializer):

    def __init__(self, realm: str, key: Optional[str] = None,
                 **kwargs) -> None:
        if key is None:
            key = accounts_app.config["SECRET_KEY"]
        super(Confirmation, self).__init__(key, salt=realm, **kwargs)

    def loads_http(
        self,
        s: Union[str, bytes],
        max_age: Optional[int] = None,
        return_timestamp: bool = False,
        salt: Optional[bytes] = None,
    ) -> Any:
        """
        Like `Confirmation.loads`, but raise HTTP exceptions with appropriate
        messages instead of `BadSignature` or `SignatureExpired`.
        """

        try:
            return self.loads(s, max_age, return_timestamp, salt)
        except BadSignature:
            raise Forbidden("Ungültiger Bestätigungslink.")
        except SignatureExpired:
            raise Forbidden("Bestätigungslink ist zu alt.")
