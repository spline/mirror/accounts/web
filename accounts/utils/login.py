# -*- coding: utf-8 -*-
from flask_login import LoginManager, current_user
from functools import wraps
from werkzeug.exceptions import Forbidden
from itsdangerous import base64_decode, base64_encode
import json
from accounts.app import accounts_app

from typing import Union, Any, Optional


class _compact_json:
    @staticmethod
    def loads(payload: Union[bytes, str, bytearray]) -> Any:
        return json.loads(payload)

    @staticmethod
    def dumps(obj: Union[list, dict, tuple], **kwargs) -> str:
        kwargs.setdefault("ensure_ascii", False)
        kwargs.setdefault("separators", (",", ":"))
        return json.dumps(obj, **kwargs)


def create_login_manager() -> LoginManager:
    login_manager = LoginManager()
    login_manager.login_message = "Bitte einloggen"
    login_manager.login_view = "login.login"

    @login_manager.user_loader
    def load_user(user_id: str) -> LoginManager:
        try:
            username, password = parse_userid(user_id)
            return accounts_app.user_backend.auth(username, password)
        except (
            accounts_app.user_backend.NoSuchUserError,
            accounts_app.user_backend.InvalidPasswordError,
        ):
            return None

    return login_manager


def create_userid(username: str, password: Optional[str]) -> bytes:
    userid = (username, password)
    return base64_encode(_compact_json.dumps(userid))


def parse_userid(value: str) -> Any:
    return _compact_json.loads(base64_decode(value))


def logout_required(f):
    @wraps(f)
    def logout_required_(*args, **kwargs):
        if current_user.is_authenticated:
            raise Forbidden(
                "Diese Seite ist nur für nicht eingeloggte Benutzer gedacht!"
            )
        return f(*args, **kwargs)

    return logout_required_
