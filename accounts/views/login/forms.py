# -*- coding: utf-8 -*-
from flask_wtf import FlaskForm as Form
from wtforms import StringField, PasswordField, validators


class LoginForm(Form):
    username = StringField("Benutzername")
    password = PasswordField("Passwort", [validators.DataRequired()])
