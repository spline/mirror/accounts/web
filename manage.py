#!/usr/bin/env python3
from argparse import ArgumentParser

from accounts.forms import RegisterForm
from accounts.utils.console import ConsoleForm, TablePrinter
from accounts.backend.mail.dummy import DummyBackend as DummyMailBackend
from accounts.models import Account
from accounts import create_app
from accounts.app import accounts_app

from typing import Iterator


class ListServices():
    """List the configured services."""

    def run(self) -> None:
        table = TablePrinter(['Name', 'URL'])
        table.output([[service.name, service.url]
                      for service in accounts_app.all_services])


class ListUsers():
    """List registered users."""

    def _get_users(self, locked: bool, only_locked: bool) -> Iterator[Account]:
        for user in accounts_app.user_backend.find():
            user_locked = user.mail.startswith('noreply-disabledaccount-')
            if user_locked and not (locked or only_locked):
                continue
            elif not user_locked and only_locked:
                continue

            yield user

    def run(self, locked: bool, only_locked: bool) -> None:
        table = TablePrinter(['Name', 'E-Mail', 'Uid'])
        table.output([[user.uid, user.mail, user.uidNumber]
                      for user in self._get_users(locked, only_locked)])


class CreateUser():
    """Register a user."""

    def run(self, username: str, mail: str,
            ignore_blacklist: bool = False, print_only: bool = False):
        form = ConsoleForm(RegisterForm, username=username, mail=mail)
        form.csrf_enabled = False
        del form.question

        if ignore_blacklist:
            accounts_app.username_blacklist = []

        if not form.validate():
            form.print_errors()
            return

        if print_only:
            accounts_app.mail_backend = DummyMailBackend(accounts_app)

        accounts_app.mail_backend.send(mail, 'mail/register.txt',
                                       username=username)

        if not print_only:
            print('Mail versandt.')


def main() -> None:
    parser = ArgumentParser(description="Spline Accounts")
    parser.add_argument('-c', '--config', dest='config', required=False)

    subparsers = parser.add_subparsers(dest="subcommand")
    subparsers.add_parser("list-services")

    list_users = subparsers.add_parser("list-users")
    list_users.add_argument('--locked', '-l', action='store_true',
                            dest='locked',  help='Include locked users.')
    list_users.add_argument('--only-locked', '-L', action='store_true',
                            dest='only_locked', help='Show ONLY locked users.')

    create_user = subparsers.add_parser("create-user")
    create_user.add_argument('--ignore-blacklist', action='store_true',
                             dest='ignore_blacklist',
                             help='Ignore blackisted user names.')
    create_user.add_argument('--print', '-p', action='store_true', dest='print_only',
                             help='Do not send the activation link via mail, only print it.'),
    create_user.add_argument(dest='username', metavar='USERNAME',
                             help='Username that should be registered.'),
    create_user.add_argument(dest='mail', metavar='MAIL',
                             help='Mail address of the new user.'),

    args = parser.parse_args()

    app = create_app(args.config)
    with app.app_context():
        if args.subcommand == "list-services":
            ListServices().run()
        elif args.subcommand == "list-users":
            ListUsers().run(args.locked, args.only_locked)
        elif args.subcommand == "create-user":
            with app.test_request_context("/cli/create-user"):
                CreateUser().run(args.username, args.mail, args.ignore_blacklist,
                                 args.print_only)


if __name__ == '__main__':
    main()
