#!/usr/bin/env bash

if [ $(whoami) != "accounts-web" ]; then
        su accounts-web bash -c "$0"
fi

source venv/bin/activate

"$(dirname $(readlink -f $0))/manage.py" $@
