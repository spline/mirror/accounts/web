#!/usr/bin/env python
from accounts import create_app

application = create_app()
application.run()
